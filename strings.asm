org 100h

jmp start

sentence db ?
opening_statement db "Enter your string: $"
preview_string db "Your string: $"

start:

push offset opening_statement
call get_str

push offset preview_string
push offset sentence
call print_str
   
mov ah, 0
int 16h
ret

get_str proc
    push bp
    mov bp, sp

    mov dx, [bp+4] ;printing opening_statement 
    mov ah, 9
    int 21h 
    
    mov dl, 10 ;printing new line
    mov ah, 02h
    int 21h
    mov dl, 13
    mov ah, 02h
    int 21h
    
    mov bx, 0 ;starting from index 0 
    
    get_string:
        mov ah, 1
        int 21h
        mov sentence[bx], al ;getting char and putting it in index bx in the sentence
        cmp al, 0Dh
        je continue ;break when user inputs '$'
        inc bx ;next index
        jmp get_string    
    
    
    continue:
    mov sentence[bx], '$' ;putting '$' instead of 'Enter' for sentence ending
    
    mov dl, 10 ;printing new line
    mov ah, 02h
    int 21h
    mov dl, 13
    mov ah, 02h
    int 21h 
     
    mov sp, bp
    pop bp
    retn 2
endp get_str 

print_str proc
    push bp
    mov bp, sp
    
    mov dx, [bp+6] ;printing preview_string
    mov ah, 9
    int 21h
    
    mov dl, 10 ;printing new line
    mov ah, 02h
    int 21h
    mov dl, 13
    mov ah, 02h
    int 21h
        
    mov dx, [bp+4] ;printing the sentence
    mov ah, 9
    int 21h
    
    mov sp, bp
    pop bp
    retn 4
endp print_str    